<table class="easyui-datagrid" 
	style="height: 340px" 
	rownumbers="true" 
	singleSelect="true" 
	pagination="true" 
    fitColumns="true"
	method="get"
	url="task_json.php" 
    id="taskGrid">
	<thead>
		<tr>
            <th field="running_no" width="40">ลำดับงาน</th>
			<th field="task_type_name" width="40">ประเภทงาน</th>
			<th field="task_status" width="50">สถานะของงาน</th>
			<th field="task_date_from" width="60">วัน/เวลาที่เริ่ม</th>
			<th field="task_date_to" width="60">วัน/เวลาที่สิ้นสุด</th>
			<th field="task_detail" width="250">รายละเอียดของงาน</th>
		</tr>
	</thead>
</table>