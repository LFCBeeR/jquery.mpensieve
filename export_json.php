<?php
    ob_start();
    session_start();

    include_once 'connect_mysql.php';
    
    $task_type_id = $_POST['task_type_id'];
	$user_id = $_SESSION['user_id'];
	$task_status = $_POST['task_status'];
	$task_date_from = $_POST['task_date_from'];
	$task_date_to= $_POST['task_date_to'];

    $arr1 = explode(" ", $task_date_from);
    $arr_d1 = explode("/", $arr1[0]);
    $time1 = $arr1[1];
    $y1 = $arr_d1[2];
    $m1 = $arr_d1[0];
    $d1 = $arr_d1[1];

    $new_task_date_from = "$y1-$m1-$d1 $time1";

    $arr2 = explode(" ", $task_date_to);
    $arr_d2 = explode("/", $arr2[0]);
    $time2 = $arr2[1];
    $y2 = $arr_d2[2];
    $m2 = $arr_d2[0];
    $d2 = $arr_d2[1];

    $new_task_date_to = "$y2-$m2-$d2 $time2";

    try {
	    
	    $stmt1 = $conn->prepare("CALL sp_GenerateCSV(:task_status,:task_date_from,:task_date_to,:user_id,:task_type_id)");
		$stmt1->bindParam(':task_status', $task_status);
		$stmt1->bindParam(':task_date_from', $$new_task_date_from);
		$stmt1->bindParam(':task_date_to', $new_task_date_to);
		$stmt1->bindParam(':user_id', $$user_id);
		$stmt1->bindParam(':task_type_id', $task_type_id);
	    
	    $stmt1->execute();
        
        $items = array();

        while($rs = $stmt1->fetch(PDO::FETCH_ASSOC)) {
            array_push($items, $rs);
        }
        $arr['rows'] = $items;

        echo json_encode($arr);
        
    } catch(PDOException $err) {
        echo $err->getMessage();
    }   

?>