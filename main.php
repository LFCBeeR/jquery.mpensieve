<?php
    ob_start();
    session_start();
    
    if(empty($_SESSION['user_id']))
    {
        header('Location: index.php');
    }
?>

<html>
    <head>
		<title>MPensieve 2016</title>
		<meta charset="UTF-8" />

		<link rel="stylesheet" href="assets/library/jquery-easyui-1.4.4/themes/default/easyui.css" />
        <link rel="stylesheet" href="assets/library/jquery-easyui-1.4.4/themes/icon.css" />
        
        <script src="assets/library/jquery.js"></script>
        <script src="assets/library/jquery-easyui-1.4.4/jquery.easyui.min.js"></script>
        <script src="assets/library/script.js"></script>

        <style> 
            form label 
            {
                width: 100px;
                text-align: right;
                padding-right: 3px;
                display: inline-block;
            }
            form div 
            {
                margin-top: 2px;
                margin-bottom: 2px;
            }
        </style>

	</head>

	<body style="margin: 0px">
        <div class="easyui-layout"
            data-options="fit: true"
            style="width: 100%; height: 350px;">
            
            <div data-options="region: 'north', collapsible: false"
                title="MPensieve 2016 version 1.0"
                style="height:70px">
                <?php include_once 'menu_top.php'; ?>
            </div>
            
            <div data-options="region: 'south', split: true"
                title="คอนโซล"
                style="height: 100px"
                id=console>
            </div>
            
            <div data-options="region: 'east', split: true"
                title="งานที่บันทึก"
                style="height: 50px; width: 150px">
	            <!--<?php include_once 'panel_right.php'; ?>-->
            </div>

            <div data-options="region: 'west', split: true"
                title="เมนู"
                style="height: 50px; width: 200px">
                <?php include_once 'menu_left.php'; ?>
            </div>

            <div data-options="region: 'center', split: true"
                title="หน้าจอหลัก"
                style="height: 50px; width: 400px"
	            id="content">
            </div>
            
        </div>
     </body>
</html>