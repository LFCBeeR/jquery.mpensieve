<?php
    include_once 'connect_mysql.php';
    
    $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
    $rows = isset($_GET['rows']) ? intval($_GET['rows']) : 10;
    $offset = ($page - 1) * $rows;
    
    $stmt1 = $con->prepare("SELECT * FROM t_task_record");
    $stmt1->execute();

    $row = $stmt1->rowCount();

    $arr["total"] = $row;

    $stmt2 = $con->prepare("
        SELECT 
            t_task_record.running_no, 
            m_task_type.task_type_name,
            IF(t_task_record.task_status = '1','Full','Partial') AS task_status,
            DATE_FORMAT(t_task_record.task_date_from,'%Y-%m-%d %H:%i') AS task_date_from,
            DATE_FORMAT(t_task_record.task_date_to,'%Y-%m-%d %H:%i') AS task_date_to,
            t_task_record.task_detail
        FROM t_task_record
            INNER JOIN m_task_type ON m_task_type.task_type_id = t_task_record.task_type_id
            INNER JOIN m_user ON m_user.user_id = t_task_record.user_id
        WHERE t_task_record.delete_flag = '0'
        GROUP BY t_task_record.running_no
        ORDER BY running_no DESC 
        LIMIT $offset, $rows");

    $stmt2->execute();

    $items = array();

    while($rs = $stmt2->fetch(PDO::FETCH_ASSOC)) {
        array_push($items, $rs);
    }
    $arr['rows'] = $items;
    
    echo json_encode($arr);

?>