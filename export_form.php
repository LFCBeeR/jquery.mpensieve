<?php
	include_once 'connect_mysql.php';    

    $stmt1 = $con->prepare("SELECT * FROM m_task_type WHERE delete_flag = '0'");
    $stmt1->execute();
    $task_type = $stmt1->fetchAll(PDO::FETCH_ASSOC);  

?>

<form id="formExportCSV" style="padding: 10px">
    <fieldset style="margin-bottom: 20px">
        <legend>กรอกเงื่อนไขการสร้างไฟล์</legend>
        <div>
            <label>ประเภทงาน: </label>
            <select id="task_type_id" class="easyui-combobox" name="task_type_id" style="width:200px;">
                    <option value="0">All</option>
                <?php foreach($task_type as $t_t_param): ?>
                    <?php if($r['task_type_id'] == $t_t_param['task_type_id']) { ?>
                        <option selected="selected" value="<?php echo $t_t_param['task_type_id']; ?>"><?php echo $t_t_param['task_type_name']; ?></option>
                    <?php } else { ?> 
                        <option value="<?php echo $t_t_param['task_type_id']; ?>"><?php echo $t_t_param['task_type_name']; ?></option>
                    <?php }; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <div>
            <label>สถานะของงาน: </label>
                <input type="radio" id="task_status" name="task_status" value="0" checked="checked" /> All
                <input type="radio" id="task_status" name="task_status" value="1" /> Full
                <input type="radio" id="task_status" name="task_status" value="2" /> Partial
        </div>
        <div>
            <label>วัน/เวลาที่เริ่ม: </label>
            <input class="easyui-datetimebox" id="task_date_from" name="task_date_from" 
                data-options="required:false,showSeconds:false" style="width:200px">
        </div>
        <div>
            <label>วัน/เวลาที่สิ้นสุด: </label>
            <input class="easyui-datetimebox" id="task_date_to" name="task_date_to" 
                data-options="required:false,showSeconds:false" style="width:200px">
        </div>
    </fieldset>
    <div style="margin:10px 0;">
        <a href="#" class="easyui-linkbutton l-btn l-btn-small" data-options="iconCls:'icon-add'" 
           group="" id="" style="float: right; width: 80px">
            <span class="l-btn-left l-btn-icon-left">
                <span class="l-btn-text">Clear</span>
                <span class="l-btn-icon icon-reload">&nbsp;</span>
            </span>
        </a>  
        <a href="#" class="easyui-linkbutton l-btn l-btn-small" data-options="iconCls:'icon-add'" 
           group="" id="generateCSV"style="float: right; width: 80px" onclick="generateCSV()">
            <span class="l-btn-left l-btn-icon-left">
                <span class="l-btn-text">Generate</span>
                <span class="l-btn-icon icon-print">&nbsp;</span>
            </span>
        </a> 
    </div>
</form>