<div class="easyui-accordion" data-options="multiple: true">
    <div title="บันทึก">
        <div style="padding: 5px">
            <div>
                <a class="easyui-linkbutton"
                    data-options="iconCls: 'icon-ok', plain: true"
                    onclick="taskRecord()">
                    บันทึกงาน
                </a>
            </div>
            <div>
                <a class="easyui-linkbutton"
                    data-options="iconCls: 'icon-ok', plain: true"
                    onclick="generateExcel()">
                    สร้างไฟล์จากงานที่บันทึก
                </a>
            </div>
        </div>
    </div>
    <div title="สถิติ">
        <div style="padding: 5px">
             <div>
                <a class="easyui-linkbutton"
                     data-options="iconCls: 'icon-ok', plain: true"
                    onclick="generateChart()">
                     สร้าง Chart
                 </a>
            </div>
        </div>
    </div>
    <div title="ตั้งค่า">
        <div style="padding: 5px">
            <div>
                <a class="easyui-linkbutton"
                    data-options="iconCls: 'icon-ok', plain: true"
                    onclick="editProfile()">
                    แก้ไขโปรไฟล์
                </a>
            </div>
            <div>
                <a class="easyui-linkbutton"
                    data-options="iconCls: 'icon-ok', plain: true"
                    onclick="eraseAllData()">
                    ลบข้อมูลทั้งหมด
                </a>
            </div>
            <div>
                <a class="easyui-linkbutton"
                    data-options="iconCls: 'icon-ok', plain: true"
                    onclick="systemAbout()">
                    เกี่ยวกับระบบ
                </a>
            </div>
        </div>
    </div>
</div>

           
                  