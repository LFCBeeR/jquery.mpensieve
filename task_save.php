<?php
    ob_start();
    session_start();

	include_once 'connect_mysql.php';	

    if(empty($_SESSION['user_id']))
    {
        header('Location: index.php');
    }
	
	$task_type_id = $_POST['task_type_id'];
	$user_id = $_SESSION['user_id'];
	$task_status = $_POST['task_status'];
	$task_date_from = $_POST['task_date_from'];
	$task_date_to = $_POST['task_date_to'];
	$task_detail = $_POST['task_detail'];

    $arr1 = explode(" ", $task_date_from);
    $arr_d1 = explode("/", $arr1[0]);
    $time1 = $arr1[1];
    $y1 = $arr_d1[2];
    $m1 = $arr_d1[0];
    $d1 = $arr_d1[1];

    $new_task_date_from = "$y1-$m1-$d1 $time1";

    $arr2 = explode(" ", $task_date_to);
    $arr_d2 = explode("/", $arr2[0]);
    $time2 = $arr2[1];
    $y2 = $arr_d2[2];
    $m2 = $arr_d2[0];
    $d2 = $arr_d2[1];

    $new_task_date_to = "$y2-$m2-$d2 $time2";

    try {
	    
	    $stmt2 = $con->prepare("
	    	SELECT t_task_record.user_id
			FROM t_task_record
			WHERE 
				user_id = $user_id
			AND
				task_date_from BETWEEN '$new_task_date_from' AND '$new_task_date_to'
			OR 
				task_date_to BETWEEN '$new_task_date_from' AND '$new_task_date_to'
			OR 
				'$new_task_date_from' BETWEEN task_date_from AND task_date_to
			OR
				'$new_task_date_to' BETWEEN task_date_from AND task_date_to
		");
	    
	    $stmt2->execute();
	    $count = $stmt2->rowCount();
	    
	    if($count == 0) {
		    
		    $stmt1 = $con->prepare("
                INSERT INTO t_task_record (task_type_id, user_id, task_status, task_date_from, task_date_to, task_detail) 
                VALUES
                (
                    '$task_type_id', 
                    '$user_id', 
                    '$task_status', 
                    '$new_task_date_from', 
                    '$new_task_date_to', 
                    '$task_detail'
                )"
            );
			
	        $stmt1->execute();
	        
	        echo 'success';
	        
	    } else {
		    echo 'duplicate';
		    
	    }
	                    
    } catch(PDOException $err) {
        echo $err->getMessage();
    }
	
?>