function login() {
    $(document).ready(function() {
        $("#login").click(function() {
            var username = $("#username").val();
            var password = $("#password").val();
            // Checking for blank fields.
            if( username == '' || password == '') {
                $('input[type="text"],input[type="password"]').css("border","2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                alert("กรุณาใส่ข้อมูลให้ครบ");
            } else {
                $.post("login.php",{ username1: username, password1: password },
                function(data) {
                    if(data=='Invalid') {
                        $('input[type="text"],input[type="password"]').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                        alert('ไม่มี Username นี้ในระบบ');
                    } else if(data=='Success') {
                        window.location.href = "main.php";
                    } else if(data=='Expire') {
                        $('input[type="text"],input[type="password"]').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                        alert('User นี้ออกจากบริษัทไปแล้ว');
                    } else {
                        alert(data);
                    }
                });
            }
        });
    });
}

function taskRecord() {
    $.ajax({
        url: 'task_index.php',
        success: function(data) {
            $("#content").html(data);
            taskList();
        }
    });
}

function taskList() {
    var grid = $("#taskGrid").datagrid({
        toolbar: [{
            text: 'New',
            iconCls: 'icon-add',
            handler: function() {
                taskFormNew();
            }
        }, {
            text: 'Edit',
            iconCls: 'icon-edit',
            handler: function() {
                taskFormEdit();
            }
        }, {
            text: 'Delete',
            iconCls: 'icon-remove',
            handler: function() {
                taskDelete();
            }
        }, {
            text: 'Export',
            iconCls: 'icon-print',
            handler: function() {
                generateExcel();
            }
        }]
    });
}

function taskFormNew() {
    var d = $("<div></div>").dialog({
        id:'task_dialog',
        title: 'ข้อมูลของงานที่จะบันทึก',
        href: 'task_form.php?id=0',
        modal: true,
        draggable: true,
        width: 330,
        height: 330,
        buttons: [{
            text: 'Save',
            iconCls: 'icon-save',
            handler: function() {
                taskSaveNew();
            }
        }, {
            text: 'Cancel',
            iconCls: 'icon-cancel',
            handler: function() {
                d.dialog("destroy");
            }
        }]
    });
    return false;
}

function taskFormEdit() {
    var row = $('#taskGrid').datagrid('getSelected');
    var id;
    
    if(row != null) {
        id = row.running_no;
    } else {
        $.messager.alert('คำเตือน','กรุณาเลือกงานที่ต้องการจะแก้ไข');
        exit();
    }
    
    var d = $("<div></div>").dialog({
        id:'task_dialog',
        title: 'ข้อมูลของงานที่จะบันทึก',
        href: 'task_form.php?id= ' + id,
        modal: true,
        draggable: true,
        width: 330,
        height: 330,
        buttons: [{
            text: 'Save',
            iconCls: 'icon-save',
            handler: function() {
                taskSaveEdit();
            }
        }, {
            text: 'Cancel',
            iconCls: 'icon-cancel',
            handler: function() {
                d.dialog("destroy");
            }
        }]
    });
    return false;
}

function taskDelete() {
    var row = $('#taskGrid').datagrid('getSelected');
    
    $.ajax({
        url: 'task_delete.php',
        data: {
            id: row.running_no
        },
        success: function(data) {
            if(data == 'success') {
                taskRecord();
                log("ลบข้อมูล task งานเรียบร้อย");
            }
        }
    });
    return false;
}

function taskSaveNew() {
    var url = 'task_save.php';
    
    $.ajax({
        url: url,
        data: $("#formTaskRecord").serialize(),
        type: 'POST',
        success: function(data) {
            if(data == 'success') {
                taskRecord();
                $('#task_dialog').dialog("destroy");
                log("บันทึกข้อมูล task งานเรียบร้อย");
            } else if(data == 'duplicate') {
                $('#duplicate_msg').text('* ช่วงเวลานี้ได้ถูกบันทึกแล้ว กรุณาเลือกช่วงเวลาใหม่');
                log("ช่วงเวลานี้ได้ถูกบันทึกแล้ว กรุณาเลือกช่วงเวลาใหม่");
            } else {
                $('#duplicate_msg').text('Error');
            }
        }
    });
}

function taskSaveEdit() {
    var row = $('#taskGrid').datagrid('getSelected');
    var url = 'task_save.php';
    
    if(row != null) {
        url = "task_edit.php?id=" + row.running_no
    }
    
    $.ajax({
        url: url,
        data: $("#formTaskRecord").serialize(),
        type: 'POST',
        success: function(data) {
            if(data == 'success') {
                taskRecord();
                $('#task_dialog').dialog("destroy");
                log("บันทึกข้อมูล task งานเรียบร้อย");
            } else if(data == 'duplicate') {
                $('#duplicate_msg').text('* ช่วงเวลานี้ได้ถูกบันทึกแล้ว กรุณาเลือกช่วงเวลาใหม่');
                log("ช่วงเวลานี้ได้ถูกบันทึกแล้ว กรุณาเลือกช่วงเวลาใหม่");
            } else {
                $('#duplicate_msg').text('Error');
            }
        }
    });
}

function log(msg) {
	$("#console")
		.css("padding", "10px")
		.text(msg);
}

function generateExcel() {
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('taskGrid'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}