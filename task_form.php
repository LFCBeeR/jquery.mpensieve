<?php
	include_once 'connect_mysql.php';    
	
	if(!empty($_GET)) {
		$id = $_GET['id'];
		$stmt1 = $con->prepare("
			SELECT t_task_record.task_detail, t_task_record.task_status, m_task_type.task_type_id,
				DATE_FORMAT(t_task_record.task_date_from,'%m/%d/%Y %H:%i') AS task_date_from,
                DATE_FORMAT(t_task_record.task_date_to,'%m/%d/%Y %H:%i') AS task_date_to
			FROM t_task_record
				INNER JOIN m_task_type ON m_task_type.task_type_id = t_task_record.task_type_id
				INNER JOIN m_user ON m_user.user_id = t_task_record.user_id
			WHERE t_task_record.running_no = $id AND t_task_record.delete_flag = '0'
			GROUP BY t_task_record.running_no
			");
		$stmt1->execute();
		$r = $stmt1->fetch(PDO::FETCH_ASSOC);

	}
		
		$stmt2 = $con->prepare("SELECT * FROM m_task_type WHERE delete_flag = '0'");
		$stmt2->execute();
		$task_type = $stmt2->fetchAll(PDO::FETCH_ASSOC);  

?>

<form id="formTaskRecord" style="padding: 5px">
	<div>
		<label>ประเภทงาน: </label>
		<select id="task_type_id" class="easyui-combobox" name="task_type_id" style="width:200px;">
				<option value="0">-- กรุณาเลือกประเภทของงาน --</option>
			<?php foreach(@$task_type as $t_t_param): ?>
				<?php if(@$r['task_type_id'] == $t_t_param['task_type_id']) { ?>
					<option selected="selected" value="<?php echo $t_t_param['task_type_id']; ?>"><?php echo $t_t_param['task_type_name']; ?></option>
				<?php } else { ?> 
					<option value="<?php echo $t_t_param['task_type_id']; ?>"><?php echo $t_t_param['task_type_name']; ?></option>
				<?php }; ?>
			<?php endforeach; ?>
		</select>
	</div>
	<div>
        <label>สถานะของงาน: </label>
        <?php if(@$r['task_status']=='1') {
	        echo('<input type="radio" id="task_status" name="task_status" value="1" checked="checked" /> Full');
	        echo('<input type="radio" id="task_status" name="task_status" value="2" /> Partial');
	    } elseif(@$r['task_status']=='2') {
	    	echo('<input type="radio" id="task_status" name="task_status" value="1" /> Full');
	        echo('<input type="radio" id="task_status" name="task_status" value="2" checked="checked" /> Partial');
		} else {
			echo('<input type="radio" id="task_status" name="task_status" value="1" checked="checked" /> Full');
	        echo('<input type="radio" id="task_status" name="task_status" value="2" /> Partial');
		};?>
    </div>
	<div>
		<label>วัน/เวลาที่เริ่ม: </label>
		<input class="easyui-datetimebox" id="task_date_from" name="task_date_from" 
        	data-options="required:false,showSeconds:false" value="<?php echo @$r['task_date_from']; ?>" style="width:150px">
	</div>
	<div>
		<label>วัน/เวลาที่สิ้นสุด: </label>
		<input class="easyui-datetimebox" id="task_date_to" name="task_date_to" 
        	data-options="required:false,showSeconds:false" value="<?php echo @$r['task_date_to']; ?>" style="width:150px">
	</div>
	<div>
		<label>รายละเอียดของงาน: </label><br>
		<textarea id="task_detail" name="task_detail" cols="40" rows="6"><?php echo @$r['task_detail']; ?></textarea>
	</div>
    <div id="duplicate_msg" style="color: red"></div>
</form>