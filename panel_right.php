<?php
    include_once 'connect_mysql.php';

    $sql = "
    	SELECT 
    		DATE_FORMAT(tb_rent.created_at, '%d/%m/%y') AS created_at,
    		DATE_FORMAT(tb_rent.will_date_send, '%d/%m/%y') AS will_date_send,
    		tb_rent.id,
    		tb_book.isbn,
    		tb_book.name,
    		tb_book.author,
    		tb_book.price,
    		tb_book_stock.barcode,
    		CONCAT(tb_member.fname, ' ', tb_member.lname) AS member_name
    	FROM tb_rent
    		LEFT JOIN tb_book_stock ON tb_book_stock.id = tb_rent.book_stock_id
    		INNER JOIN tb_book ON tb_book.id = tb_book_stock.book_id
    		INNER JOIN tb_member ON tb_member.id = tb_rent.member_id
    	WHERE send = 'no'
    	ORDER BY will_date_send DESC";
    
    $rs = mysql_query($sql);
    $arr = array();
    
    while ($r = mysql_fetch_assoc($rs)) { ?>
    <div style="padding: 1px">
    	<a href="#"
    		class="easyui-linkbutton"
    		plain="true"
    		onclick="showBookNoSend( <?php echo $r['id']; ?> )">
    		<span class="tree-icon tree-file"></span>
    		<?php echo $r['name']; ?>
    	</a>
    </div>
    
<?php } ?>