<?php
    if(empty($_SESSION['user_id']))
    {
        header('Location: index.php');
    }
    
    $user_id = $_SESSION['user_id'];
    $fname_th = $_SESSION['fname_th'];
    $sname_th = $_SESSION['sname_th'];
    
?>

<div>
    <div style="float: left;padding: 10px">
        <label>ยินดีต้อนรับ </label>
        <label><?php echo $fname_th; ?></label> <label><?php echo $sname_th; ?></label>
        รหัสประจำตัวพนักงาน
        <label><?php echo $user_id; ?></label>
        เข้าสู่ระบบ
    </div>
    <div style="float: right;padding: 7px">
        <a href="logout.php" class="easyui-linkbutton" plain="true" iconCls="icon-cancel">ออกจากระบบ</a>
    </div>     
    <div style="clear: both"></div>
</div>